#include <EtherCard.h>
#include <RingBuf.h>
// #include <Adafruit_NeoPixel.h>
// Adafruit_NeoPixel led = Adafruit_NeoPixel(1, 4, NEO_GRB + NEO_KHZ800);

struct Notification {
    char* type;
    unsigned int interval = 0;
    unsigned long lastMillis = 0;
    bool active = true;
    bool dismissable = false;
    unsigned int cycles = 0;
    unsigned int maxCycles = 1;
};
struct Event {
    char* type;
    unsigned int id;
    unsigned long lastLogged = 0;
    bool logged = false;
    bool active = true;
    uint8_t retries = 0;
};
unsigned int eventId = 0;

RingBuf *notification = RingBuf_new(sizeof(struct Notification), 3);
RingBuf *event = RingBuf_new(sizeof(struct Event), 3);

const uint8_t doorbellPin = 2;
const uint8_t dismissAudioPin = 3;

const unsigned int postTimeout PROGMEM = 10000;

volatile boolean interruptDebounced[] = {false, false};

volatile unsigned long previousMillis;

static byte macaddress[] = { 0x74,0x69,0x69,0x2D,0x30,0x31 };
static byte session;
byte Ethernet::buffer[300];
Stash stash;

const char website[] PROGMEM = "homebase";



void setup () {

    // led.begin();
    // led.setBrightness(50);
    // led.setPixelColor(0,0,255,0);
    // led.show();

    attachInterrupt(digitalPinToInterrupt(doorbellPin), doorbellISR, FALLING);

    Serial.begin(57600);

    if (!notification || !event) {
        // Serial.println("No memory");
        while (1);
    }
    if (ether.begin(sizeof Ethernet::buffer, macaddress) == 0)
        Serial.println(F("Ethernet failed"));
    if (!ether.dhcpSetup()) {
        Serial.println(F("DHCP failed"));
    }
    else {
        ether.printIp("SRV: ", ether.hisip);
    }
    ether.printIp("IP:  ", ether.myip);
    ether.printIp("GW:  ", ether.gwip);
    ether.printIp("DNS: ", ether.dnsip);

    if (!ether.dnsLookup(website)) {
        Serial.println(F("DNS failed"));
    }

    delay(2000);
}

void loop () {

    interruptDebounce();
    handleNotifications();
    handleEvents();
    getResponse();

    if (!notification || !event) {
        Serial.println("No memory");
        while (1);
    }

    if (Serial.available() > 0) {

        char inChar = (char)Serial.read();

        if (inChar == 'e') {
            print_event_contents();
        }
        if (inChar == 'n') {
            print_notification_contents();
        }
        //empty serial buffer
        while (Serial.read() > 0);
    }
}

void handleNotifications() {
    for(int i = 0; i < notification->numElements(notification);i++){
        struct Notification* n = notification->peek(notification,i);

        if(n->type == "audio" && n->active == true) {

            if(n->lastMillis == 0 || millis() - n->lastMillis > n->interval) {
                if (n->cycles > 2 && dismissAudio()) {
                    notification->pull(notification, &n);
                    break;
                }
                soundNotification();
                n->lastMillis = millis();
                n->cycles++;
                if(n->cycles >= n->maxCycles)
                    n->active = false;
            }
        }
        else if (n->type == "led-red") {
            // led.setPixelColor(0,255,0,0);
            // led.show();
        }
        else if (n->type == "led-blue") {
            // led.setPixelColor(0,0,0,255);
            // led.show();
        }
    }
}

bool dismissAudio() {
    if (digitalRead(dismissAudioPin) == HIGH) {
        return true;
    }
    else {
        return false;
    }
}

void handleEvents() {
    for(int i = 0; i < event->numElements(event);i++){
        struct Event* e = event->peek(event,i);
        if (e->active) {
            if (!e->logged || millis() - e->lastLogged > postTimeout) {
                // timedout
                Serial.println("Logging  event: pos = " + String(i) + " id = " + String(e->id));
                postRequest(i);
            }

        }
        else {
            if (millis() - e->logged > 5000) { // delete after 5 seconds
                Serial.println("Popping event: pos = " + String(i) + " id = " + String(e->id));
                event->pull(event, &e);
                break;
            }
        }
    }
}
void interruptDebounce() {

    if(interruptDebounced[0]){
        delay(5);
        if(!digitalRead(doorbellPin)){
            interruptDebounced[0] = false;
            addNotification("audio", 1000, 10, true);
            addNotification("led-blue", 0, 0, false);
            addEvent("bell");
        }
    }
}

void doorbellISR() {

    Serial.println("ISR");
     if(!interruptDebounced[0] && millis() - previousMillis > 10000){ // register bell events more thatn 2 sec appart
        Serial.println("Debounced");

        interruptDebounced[0] = true;
        previousMillis = millis();
     }
     else {
        Serial.println("Interval too small: " + String(millis() - previousMillis));
     }

}

void addEvent(char* type) {

    struct Event e;

    e.type = type;
    e.id = eventId;

    event->add(event, &e);
    Serial.println("Event added ID: " + String(e.id));
    eventId++;
}

void addNotification(char* type, unsigned int interval, unsigned int maxCycles, bool dismissable) {

    struct Notification n;

    n.type = type;
    n.dismissable = dismissable;
    n.interval = interval;
    n.maxCycles = maxCycles;
    int id = notification->add(notification, &n);
    Serial.println("notification ID : " + String(id));
}

static void postRequest (int bufPos) {

    struct Event* e = event->peek(event,bufPos);

    Serial.println("post request, event: id = " + String(e->id) + " pos = " + String(bufPos));
    byte sd = stash.create();

    stash.print("data=");
    stash.print(e->type);
    stash.print("&id=");
    stash.println(e->id);
    stash.save();
    int stash_size = stash.size();

    Stash::prepare(PSTR("POST /test.php HTTP/1.1" "\r\n"
        "Host: $F" "\r\n"
        "Content-Type: application/x-www-form-urlencoded;" "\r\n"
        "Content-Length: $D" "\r\n"
        "\r\n"
        "$H"),
        website, stash_size, sd);

    session = ether.tcpSend();
    e->lastLogged = millis();
    e->logged = true;
}

void soundNotification() {

        tone(7, 500, 25);
        delay(50);
        noTone(7);
        tone(7, 1200, 25);
        delay(50);
        noTone(7);
        tone(7, 500, 25);
        delay(50);
        noTone(7);
}

int getResponse() {

    // Serial.println("getResponse");

    ether.packetLoop(ether.packetReceive());
    const char* reply = ether.tcpReply(session);

    if (reply != 0) {

        String responseCode = "";
        bool responseStatus = false;
        boolean responseCodeFound = false;
        int key;

        Serial.print("Response length:");
        Serial.println(strlen(reply));
        Serial.println("Response:");
        Serial.println(reply);
        Serial.println("----");

        for(int i = 0 ; i < strlen(reply); i++) {

            if(reply[i] == '@' && reply[i+4] == '#' && reply[i+6] == '@') {
                responseCodeFound = true;
                key = i;
                break;
            }
        }
        if(responseCodeFound){
            responseCode += reply[key+1];
            responseCode += reply[key+2];
            responseCode += reply[key+3];

            if (reply[key+5] == '1') {
                handleEvent(responseCode.toInt(), true);
            }
            else {
                handleEvent(responseCode.toInt(), false);
            }

        }
        Serial.print("Response code: ");
        Serial.println(responseCode);


        return responseCode.toInt();
    }
}

void handleEvent(int id, bool status){
    Serial.println("Deactivator");
    for(int i = 0; i < event->numElements(event);i++){
        struct Event* e = event->peek(event,i);
        if (e->id == id) {

            if (status) {
                e->active = false;
                Serial.println("Deactivated event id: " + String(e->id) + " pos = " + String(i));
                return;
            }
            else {
                if (e->retries > 5) {
                    e->active = false;
                    addNotification("led-red", 0, 0, false);
                    return;
                }
                e->retries++;
            }
        }
    }
}

void print_event_contents() {

    struct Event e;

    Serial.println("\nDumping Events");

    // Keep looping until pull() returns NULL
    while (event->pull(event, &e)) {

        Serial.print("id: ");
        Serial.println(e.id);

        Serial.print("type: ");
        Serial.println(e.type);

        Serial.print("lastLogged: ");
        Serial.println(e.lastLogged);

        Serial.print("active: ");
        Serial.println(e.active);
        Serial.println();
    }

    Serial.println("Done");

}
void print_notification_contents() {

    struct Notification n;

    Serial.println("\nDumping Notifications");

    // Keep looping until pull() returns NULL
    while (notification->pull(notification, &n)) {

        Serial.print("type: ");
        Serial.println(n.type);

        Serial.print("active: ");
        Serial.println(n.active);
        Serial.println();
    }

    Serial.println("Done");

}
